﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectControllers : MonoBehaviour,IDisableObjectInSomeTime
{
    [SerializeField]
    private float secondsTillDisabling;

    private float startTime;

    private void Awake()
    {
        startTime = secondsTillDisabling;
    }

    public IEnumerator DisableObject()
    {
        yield return new WaitForSeconds(secondsTillDisabling);
        secondsTillDisabling = startTime;
        this.gameObject.SetActive(false);
    }
}
