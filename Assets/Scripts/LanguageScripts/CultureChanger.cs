﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using TMPro;
using System.Threading;

public enum LanguageToUse
{
    Russian,
    Romanian,
    English
}


public class CultureChanger : MonoBehaviour
{


    [SerializeField]
    protected LanguageToUse langToUse;
    [SerializeField]
    protected TextMeshProUGUI dayName;

    private CultureInfo ci;
    private DateTime day;

    private string formatedDayString;
    private int enumSwitcher = 0;


    [SerializeField]
    private MaterialSkinData[] matSkinData;
    private void Awake()
    {
        ChangeMatColor();
    }

    protected void LanguageChanger()
    {
        
        switch (langToUse)
        {

            case LanguageToUse.Russian:
                ci = new CultureInfo("ru");
                Thread.CurrentThread.CurrentCulture = ci;

                formatedDayString = DateTimeFormatInfo.CurrentInfo.GetDayName(day.DayOfWeek).Substring(0, 3);
                
                dayName.text = formatedDayString.ToUpper();

                print((DateTime.Now.ToString("D", ci)));
                print(DateTimeFormatInfo.CurrentInfo.GetDayName(day.DayOfWeek));
                break;
            case LanguageToUse.Romanian:
                ci = new CultureInfo("ro");
                Thread.CurrentThread.CurrentCulture = ci;
                formatedDayString = DateTimeFormatInfo.CurrentInfo.GetDayName(day.DayOfWeek).Substring(0, 3);

                dayName.text = formatedDayString.ToUpper();

                print((DateTime.Now.ToString("D", ci)));
                print(DateTimeFormatInfo.CurrentInfo.GetDayName(day.DayOfWeek));
                break;
            case LanguageToUse.English:
                ci = new CultureInfo("en-GB");
                Thread.CurrentThread.CurrentCulture = ci;

                formatedDayString = DateTimeFormatInfo.CurrentInfo.GetDayName(day.DayOfWeek).Substring(0, 3);

                dayName.text = formatedDayString.ToUpper();

                print((DateTime.Now.ToString("D", ci)));
                print(DateTimeFormatInfo.CurrentInfo.GetDayName(day.DayOfWeek));
                break;
            default:
                print("Something happened. No Language was Choosen");
                ci = new CultureInfo("ru");
                break;
        }

  
    }

    public void SwitchLanguages()
    {
        switch (enumSwitcher)
        {
            case 0:
                langToUse = LanguageToUse.Romanian;
                LanguageChanger();
                ChangeMatColor();
                enumSwitcher++;
                break;
            case 1:
                langToUse = LanguageToUse.English;
                LanguageChanger();
                ChangeMatColor();
                enumSwitcher++;
                break;
            case 2:
                langToUse = LanguageToUse.Russian;
                LanguageChanger();
                ChangeMatColor();
                enumSwitcher = 0;
                break;
        }
        
    }

    public void SwitchLanguagesBack()
    {
        switch (langToUse)
        {
            case LanguageToUse.Romanian:
                langToUse = LanguageToUse.Russian;
                LanguageChanger();
                enumSwitcher = 2;
                ChangeMatColor();
                break;
            case LanguageToUse.English:
                langToUse = LanguageToUse.Romanian;
                LanguageChanger();
                enumSwitcher = 0;
                ChangeMatColor();
                break;
            case LanguageToUse.Russian:
                langToUse = LanguageToUse.English;
                LanguageChanger();
                enumSwitcher = 1;
                ChangeMatColor();
                break;
        }

    }

    public void ChangeMatColor()
    {
        matSkinData[enumSwitcher].GetMaterial().color = matSkinData[enumSwitcher].GetColor();
      
    }

    protected TimeSpan GetTimezoneTimeSpan()
    {
        TimeSpan timeSpan;

        if(langToUse == LanguageToUse.English)
        {
            timeSpan = DateTime.UtcNow.TimeOfDay;
            return timeSpan;
        }
        else if(langToUse == LanguageToUse.Russian)
        {
            timeSpan = DateTime.UtcNow.AddHours(3).TimeOfDay;
            return timeSpan;
        }
        else if (langToUse == LanguageToUse.Romanian)
        {
            timeSpan = DateTime.UtcNow.AddHours(1).TimeOfDay;
            return timeSpan;
        }

        return timeSpan;
    }

    protected DateTime GetTimezoneDateTime()
    {
        DateTime dateTime = DateTime.UtcNow;

        if (langToUse == LanguageToUse.English)
        {
            dateTime = DateTime.UtcNow;
            return dateTime;
        }
        else if (langToUse == LanguageToUse.Russian)
        {
            dateTime = DateTime.UtcNow.AddHours(3);
            return dateTime;
        }
        else if (langToUse == LanguageToUse.Romanian)
        {
            dateTime = DateTime.UtcNow.AddHours(1);
            return dateTime;
        }

        return dateTime;
    }


    //CultureInfo ci = new CultureInfo("ru");
    //print((DateTime.Now.ToString("D", ci)));
    //    ci = new CultureInfo("sv-SE");
    //print((DateTime.Now.ToString("D", ci)));
}
