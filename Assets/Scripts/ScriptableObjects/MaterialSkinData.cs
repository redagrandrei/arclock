﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New MaterialSkinData", menuName = "Mat Skin Data", order = 51)]
public class MaterialSkinData : ScriptableObject
{

    [SerializeField]
    private Material neededMaterial;
    [SerializeField]
    private Color colorToChange; 

    public Material GetMaterial()
    {
        return neededMaterial;
    }

    public Color GetColor()
    {
        return colorToChange;
    }

}
