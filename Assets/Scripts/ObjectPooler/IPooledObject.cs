﻿using UnityEngine;


// Additional Interface For Pooled Objects
public interface IPooledObject  {
	 /// <summary>
	/// These functions are called when the object becomes enabled,detroyed,reseted or/and active.
	/// </summary>
	void OnEnable();

    void OnDisable();

	void ResetPosition(Transform resetedPos);

	void DisableSomeComponent<T>(T component);

}
