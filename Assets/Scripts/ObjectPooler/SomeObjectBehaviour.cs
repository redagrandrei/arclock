﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomeObjectBehaviour : MonoBehaviour,IPooledObject {

	[HideInInspector]
	public float timer = 5;

	Transform resetedPosition ;

	private void Awake() {
	
	}


	public void OnEnable() {
		Debug.Log("Object Was Enabled.");
		GetComponent<Rigidbody>().AddExplosionForce(2,transform.position,2);
		
		
	}

	public void OnDisable() {
		Debug.Log("Object Was Disabled.");
		if(timer <= 0) {
			gameObject.SetActive(false);
		}
	}

	public void ResetPosition(Transform resetedPosition) {

			this.resetedPosition = resetedPosition;
			InvokeRepeating("StartReseting",0,timer);
			Debug.Log("Reseting position. ");
		
			

		
	}
	

	private void StartReseting() {
		this.transform.position = resetedPosition.position;
	}

	

	public void DisableSomeComponent<T>(T component) { // Theoreticaly can take any type of the object
		Debug.Log("Disabling some component. ");
	}
	

	
	
}
