﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfObjects {
	Ball
}

public class ObjectPooler : MonoBehaviour {

	public List<Pool> pools; //Creating public pools of different objects
	
	public Dictionary<TypeOfObjects,Queue<GameObject>> poolDictionary; //Dictionary with Pools of objects.
	public static ObjectPooler Instance; //Creating Singleton for easy access 

	


	void Awake () {


        if(Instance == null)
        {
            Instance = this;
        }else
        {
            Destroy(this.gameObject);
        }
		
		poolDictionary = new Dictionary<TypeOfObjects, Queue<GameObject>>(); //Assigning dictionary

		foreach(Pool pool in pools) {
			Queue<GameObject> objectPool = new Queue<GameObject>(); // Creating a temporary pool in for loop

			HowMuchObjectToSpawn thisObjBehaviour =  pool.objectParent.GetComponent<HowMuchObjectToSpawn>();

			if(thisObjBehaviour.howMuchToSpawn > pool.size) { // check the quantity to spawn and add more if needed
				pool.size = thisObjBehaviour.howMuchToSpawn;
			}  

			for(int i = 0; i < pool.size; i++) {
				GameObject obj = Instantiate(pool.prefab);  //Instantiating the object
				obj.transform.parent = pool.objectParent.transform;
				obj.SetActive(false); 
				objectPool.Enqueue(obj); // adding it to temporary pool
			}

			poolDictionary.Add(pool.typeOfObject, objectPool);  // adding temporary pool to our Dictionary
			
		}
		
	}

#region SPAWNING_SOMETHING_FROM_POOL
	public GameObject SpawnFromPool(TypeOfObjects typeOfObjects, Vector3 position, Quaternion rotation, int quantityToPool) { //TODO implement few bools to take control over objects activity(reset,respawn,destroy and so on)
		
		if(!poolDictionary.ContainsKey(typeOfObjects)) {
			Debug.LogError("Warning, pool with tag " + tag + " doesn't exist!");
			return null;
		}

			GameObject objectToSpawn = poolDictionary[typeOfObjects].Dequeue(); // pulling out first element in the queue

			objectToSpawn.SetActive(true);
			objectToSpawn.transform.position = position;
			objectToSpawn.transform.rotation = rotation;

			IPooledObject pooledObj = objectToSpawn.GetComponent<IPooledObject>();


			if(pooledObj != null) {
				
				pooledObj.OnEnable();

				foreach(Pool pool in pools) { // foreach object do Something   
					//pooledObj.ResetPosition(pool.resetedPosition);
				}

				
			}
		
			poolDictionary[typeOfObjects].Enqueue(objectToSpawn); // adding the object in the end of the queue,
			return objectToSpawn;
		 //TODO how many objects to pull?  

	}
#endregion
	
	
}

[System.Serializable]
public class Pool {
	public TypeOfObjects typeOfObject;
	public Transform resetedPosition;
	public GameObject objectParent;
	public GameObject prefab;
	[HideInInspector]
	public int size = 0; //Initial quantity
}
