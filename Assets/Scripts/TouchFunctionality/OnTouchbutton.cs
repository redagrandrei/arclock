﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTouchbutton : CheckTouch
{
    [SerializeField]
    private UnityEvent onTouchEvent;
 

    // Update is called once per frame
    void Update()
    {
        if (OnTouch(this.gameObject.name))
        {
            onTouchEvent.Invoke();
            AudioManager.Instance.PlayAudioOnSomePoint("Click", this.gameObject, 0.5f, true);
        }
    }

  
   
}
