﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTouchWatch : CheckTouch
{
    [SerializeField]
    private Transform ballPosition;
    private void Update()
    {
        if (OnTouch(this.gameObject.name))
        {
            GetSomeBalls();
        }
    }


    public void GetSomeBalls()
    {
        GameObject tempBall = ObjectPooler.Instance.SpawnFromPool(TypeOfObjects.Ball, this.gameObject.transform.position, Quaternion.identity, 1);
        if(ballPosition!= null)
        {
            tempBall.transform.position = ballPosition.position;
        }
        
        tempBall.GetComponent<Rigidbody>().AddForce(new Vector3(0, 20, 0),ForceMode.Impulse);
        StartCoroutine(tempBall.GetComponent<ObjectControllers>().DisableObject());
    }
   

}

