﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CheckTouch : MonoBehaviour
{
    public bool OnTouch(string whatColiderToCheck)
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (hit.collider.gameObject.transform.name == whatColiderToCheck)
                    {

                        // Do Something Here
                        return true;
                    }

                }
            }

        }
        return false;
    }
}