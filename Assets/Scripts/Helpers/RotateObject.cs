﻿using UnityEngine;

public class RotateObject : MonoBehaviour
{

    [SerializeField] private Vector3 m_Rotation = Vector3.forward;

    private void Update()
    {
        transform.Rotate(m_Rotation, Space.Self);
    }
}
