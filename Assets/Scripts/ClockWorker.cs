﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ClockWorker : CultureChanger
{   
    [SerializeField]
    private Transform hours, minutes, seconds;

    private const float hoursFloat = 360 / 12f;
    private const float minutesFloat = 360 / 60f;
    private const float secondsFloat = 360 / 60f;

    [SerializeField]
    private bool analogClock = false; 

    
    void Update()
    {
        ClockMovement();
        LanguageChanger();
    }

    private void ClockMovement()
    {
        if (analogClock)
        {
            TimeSpan timeSpan = GetTimezoneTimeSpan();
            
            if(hours != null)
                hours.localRotation = Quaternion.Euler((float)timeSpan.TotalHours * hoursFloat, 0f, 0);
            if (minutes != null)
                minutes.localRotation = Quaternion.Euler((float)timeSpan.TotalMinutes * minutesFloat, 0f, 0);
            if (seconds != null)
                seconds.localRotation = Quaternion.Euler((float)timeSpan.TotalSeconds * secondsFloat, 0f, 0);

        }
        else
        {
            DateTime time = GetTimezoneDateTime();

            if (hours != null)
                hours.localRotation = Quaternion.Euler(time.Hour * hoursFloat, 0f, 0);
            if (minutes != null)
                minutes.localRotation = Quaternion.Euler(time.Minute * minutesFloat, 0f, 0);
            if (seconds != null)
                seconds.localRotation = Quaternion.Euler(time.Second * secondsFloat, 0f, 0);
        }
    }
}
