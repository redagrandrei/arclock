﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

	public static AudioManager Instance; 

	[SerializeField]
	private SingleAudioEntity[] allAudio;



	// Use this for initialization
	void Awake ()
    {		
		if(Instance == null) {
			Instance = this;
			

		}else {
			Destroy(gameObject);
		}

        AddAudioSources();
    }

    private void AddAudioSources()
    {
        for (int i = 0; i < allAudio.Length; i++)
        {
            AudioSource tempVar = gameObject.AddComponent<AudioSource>();
            tempVar.clip = allAudio[i].currentSoundClip;
			
			if(allAudio[i].playOnAwake) {
				tempVar.Play();
			}
        }
    }


	public void PlayAudioOnSomePoint(string soundToPlay,  GameObject someObj, float volume, bool randomizePitch) {
		 for (int i = 0; i < allAudio.Length; i++) {
			 if(allAudio[i].audiotag == soundToPlay) {
				        AudioSource tempVar = someObj.AddComponent<AudioSource>();
            			tempVar.clip = allAudio[i].currentSoundClip;
						tempVar.volume = volume;
						if(randomizePitch) {
							tempVar.pitch = Random.Range(0.9f,1.1f);
						}
						
						tempVar.Play();
			 }
		 }
	}


}

[System.Serializable]
public class SingleAudioEntity {

	public string audiotag; 

	public bool playOnAwake = false;
	public AudioClip currentSoundClip; 


}
