﻿Shader "Vignete/VignetePost" {
	Properties{
	  _MainTex("Main Texture", 2D) = "white" {}
	   _VigneteRadius("Vignette Radius", Range(0.0, 1.0)) = 1.0
	  _VigneteSoft("Vignette Softness", Range(0.0, 1.0)) = 0.5

	  _NoiseScale("Noise Scale",Float) = 5.0
	  _Strength("Noise Strength",Float) = 1.0
	}

		SubShader{
		  Pass {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#include "UnityCG.cginc" 

			
			sampler2D _MainTex;
			float _VigneteRadius;
			float _VigneteSoft;

			float _NoiseScale;
			float _Strength;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			float random(float2 p)
			{
				return frac(sin(dot(p.xy, float2(_Time.y, 65.115)))*2773.8856);
			}

			float4 frag(v2f_img input) : COLOR {

				float2 normUV = input.uv;
				normUV *= _NoiseScale; 
				float2 ipos = floor(normUV);  
				float rand = random(ipos);
				fixed4 col = fixed4(rand, rand, rand, 0);
				col = lerp(tex2D(_MainTex, input.uv), col, _Strength);


				float4 base = tex2D(_MainTex, input.uv);

				float distFromCenter = distance(input.uv.xy, float2(0.5, 0.5));
				float vignette = smoothstep(_VigneteRadius, _VigneteRadius - _VigneteSoft, distFromCenter);
				base = saturate(base * vignette);
				return base * col;
			  }
			  ENDCG
} }}